package com.abde.hooptest.data.repository

import com.abde.hooptest.data.model.ActivityModel
import com.abde.hooptest.domain.model.Activity
import io.reactivex.Completable
import io.reactivex.Single

interface ActivityDataStore {
    fun getActivities(): Single<List<ActivityModel>>
    fun saveActivities(activities: List<ActivityModel>): Completable
    fun getActivity(activityId: String): Single<ActivityModel>
}