package com.abde.hooptest.data.source

import com.abde.hooptest.data.model.ActivityModel
import retrofit2.Call
import retrofit2.http.GET

interface ActivitiesService {
    @GET("test.json")
    fun getActivities(): Call<List<ActivityModel>>
}