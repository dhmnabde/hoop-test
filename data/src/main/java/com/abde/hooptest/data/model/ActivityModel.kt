package com.abde.hooptest.data.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ActivityModel : RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String = ""
    @SerializedName("startDate")
    var time: String = ""
    @SerializedName("name")
    var name: String = ""
    @SerializedName("imageURL")
    var imageUrl: String = ""
    @SerializedName("ages")
    var ages: String = ""
    @SerializedName("placeName")
    var place: String = ""
    @SerializedName("category")
    var category: String = ""
    @SerializedName("description")
    var description: String = ""
    @SerializedName("address")
    var address: AddressModel? = null
}