package com.abde.hooptest.data.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class AddressModel : RealmObject() {
    @SerializedName("latitude")
    var latitude: String = ""
    @SerializedName("longitude")
    var longitude: String = ""
    @SerializedName("country")
    var country: String = ""
    @SerializedName("postcode")
    var postcode: String = ""
    @SerializedName("streetName")
    var streetName: String = ""
    @SerializedName("town")
    var town: String = ""
}