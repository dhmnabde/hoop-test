package com.abde.hooptest.data.mapper

import com.abde.hooptest.data.model.ActivityModel
import com.abde.hooptest.domain.model.Activity
import javax.inject.Inject

class ActivityMapper @Inject constructor() : Mapper<ActivityModel, Activity> {
    override fun mapFromEntity(type: ActivityModel): Activity {
        return Activity(type.id,
                type.time,
                type.name,
                type.imageUrl,
                type.ages,
                type.place,
                type.category,
                type.description,
                type.address?.latitude ?: "",
                type.address?.longitude ?: "",
                type.address?.country ?: "",
                type.address?.postcode ?: "",
                type.address?.streetName ?: "",
                type.address?.town ?: "")
    }

    override fun mapToEntity(type: Activity): ActivityModel {
        return ActivityModel()
    }
}