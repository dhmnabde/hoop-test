package com.abde.hooptest.data.repository

import com.abde.hooptest.data.ActivityDataStoreFactory
import com.abde.hooptest.data.mapper.ActivityMapper
import com.abde.hooptest.domain.model.Activity
import com.abde.hooptest.domain.repository.ActivityRepository
import io.reactivex.Single
import javax.inject.Inject

class ActivityDataRepository @Inject constructor(private val factory: ActivityDataStoreFactory,
        private val mapper: ActivityMapper) : ActivityRepository {

    override fun getActivities(): Single<List<Activity>> {
        return factory.retrieveRemoteDataStore().getActivities()
                .flatMap {
                    return@flatMap factory.retrieveCacheDataStore().saveActivities(it).andThen(
                            factory.retrieveCacheDataStore().getActivities())
                }.map {
                    return@map it.map { mapper.mapFromEntity(it) }
                }
    }

    override fun getActivity(activityId: String): Single<Activity> {
        return factory.retrieveCacheDataStore().getActivity(activityId).map {
            mapper.mapFromEntity(it)
        }
    }
}