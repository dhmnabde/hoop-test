package com.abde.hooptest.data

import com.abde.hooptest.data.repository.ActivityDataStore
import com.abde.hooptest.data.source.ActivityCacheDataStore
import com.abde.hooptest.data.source.ActivityRemoteDataStore
import javax.inject.Inject

class ActivityDataStoreFactory @Inject constructor(
        private val activityCacheDataStore: ActivityCacheDataStore,
        private val activityRemoteDataStore: ActivityRemoteDataStore) {

    fun retrieveCacheDataStore(): ActivityDataStore {
        return activityCacheDataStore
    }

    fun retrieveRemoteDataStore(): ActivityDataStore {
        return activityRemoteDataStore
    }
}