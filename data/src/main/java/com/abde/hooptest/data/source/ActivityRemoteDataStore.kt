package com.abde.hooptest.data.source

import android.accounts.NetworkErrorException
import android.content.Context
import com.abde.hooptest.data.model.ActivityModel
import com.abde.hooptest.data.repository.ActivityDataStore
import io.reactivex.Completable
import io.reactivex.Single
import java.io.IOException
import javax.inject.Inject

class ActivityRemoteDataStore @Inject constructor(private val serviceGenerator: ServiceGenerator,
        private val context: Context) : ActivityDataStore {
    override fun getActivities(): Single<List<ActivityModel>> {
        return Single.create {
            val activitiesService = serviceGenerator.createService(ActivitiesService::class.java,
                    context)
            val getActivities = activitiesService.getActivities()
            try {
                val response = getActivities.execute()
                if (response.isSuccessful) {
                    val activitiesList = response.body()
                    if (activitiesList != null) {
                        return@create it.onSuccess(activitiesList)
                    }
                }
                return@create it.onError(NetworkErrorException(response.message()))
            } catch (e: IOException) {
                it.onError(NetworkErrorException(e.localizedMessage, e))
            } catch (e: RuntimeException) {
                it.onError(NetworkErrorException(e.localizedMessage, e))
            }
        }
    }

    override fun saveActivities(activities: List<ActivityModel>): Completable {
        throw NotImplementedError("Not implemented for remote.")
    }

    override fun getActivity(activityId: String): Single<ActivityModel> {
        throw NotImplementedError("Not implemented for remote.")
    }
}