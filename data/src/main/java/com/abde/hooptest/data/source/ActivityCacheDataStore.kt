package com.abde.hooptest.data.source

import com.abde.hooptest.data.model.ActivityModel
import com.abde.hooptest.data.repository.ActivityDataStore
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.realm.Realm
import timber.log.Timber
import java.util.Collections
import javax.inject.Inject

class ActivityCacheDataStore @Inject constructor() : ActivityDataStore {

    private val ID_KEY = "id"

    private var realm: Realm? = null

    private fun before() {
        realm = Realm.getDefaultInstance()
    }

    private fun after() {
        realm?.close()
    }

    override fun getActivities(): Single<List<ActivityModel>> {
        return Single.create { it: SingleEmitter<List<ActivityModel>> ->
            val realmResult = realm?.where(ActivityModel::class.java)
                    ?.findAll()
            if (realmResult != null) {
                Timber.d("Found activities : %s", realmResult)
                return@create it.onSuccess(
                        realm?.copyFromRealm(realmResult) ?: Collections.emptyList())
            } else {
                return@create it.onSuccess(Collections.emptyList())
            }
        }.doOnSubscribe {
            before()
        }.doAfterTerminate {
            after()
        }
    }

    override fun saveActivities(activities: List<ActivityModel>): Completable {
        return Completable.create {
            realm?.beginTransaction() ?: return@create it.onError(
                    IllegalStateException("Realm not instantiate"))
            activities.forEach {
                realm?.insertOrUpdate(it)
            }
            realm?.commitTransaction()
            Timber.d("Add activities to DB : %s", activities)
            it.onComplete()
        }.doOnSubscribe {
            before()
        }.doAfterTerminate {
            after()
        }
    }

    override fun getActivity(activityId: String): Single<ActivityModel> {
        return Single.create { it: SingleEmitter<ActivityModel> ->
            val realmResult = realm?.where(ActivityModel::class.java)
                    ?.equalTo(ID_KEY, activityId)
                    ?.findAll()
            val result = realmResult?.first()
            if (result != null) {
                Timber.d("Found activity with id: %s ->\n%s", activityId, result)
                return@create it.onSuccess(result)
            } else {
                return@create it.onError(NullPointerException("Activity not found"))
            }
        }.doOnSubscribe {
            before()
        }.doAfterTerminate {
            after()
        }
    }
}