package com.abde.hooptest.data.source

import android.content.Context
import com.abde.hooptest.data.BuildConfig
import com.abde.hooptest.data.R
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ServiceGenerator @Inject constructor() {

    private val TIMEOUT_SEC: Long = 10

    private val httpLoggingInterceptor: HttpLoggingInterceptor

    init {
        httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    fun <S> createService(serviceClass: Class<S>, context: Context): S {

        val okHttpClient = OkHttpClient().newBuilder()
                .addInterceptor {
                    val originalRequest = it.request()

                    val builder = originalRequest.newBuilder()
                            .addHeader("Authorization", "")
                            .addHeader("KeyId", "60f0c90f-992e-41d8-acf7-6ec139e87a09")

                    val newRequest = builder.build()
                    it.proceed(newRequest)
                }
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create(serviceClass)
    }
}