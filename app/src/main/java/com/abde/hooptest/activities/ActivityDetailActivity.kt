package com.abde.hooptest.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.abde.hooptest.R
import com.abde.hooptest.common.BaseActivity
import com.abde.hooptest.model.ActivityView
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail_activity.imageview_detail_map
import kotlinx.android.synthetic.main.activity_detail_activity.imageview_detail_thumbnail
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_address
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_age
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_category
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_date
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_description
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_name
import kotlinx.android.synthetic.main.activity_detail_activity.textview_detail_place
import kotlinx.android.synthetic.main.activity_list_activities.loading_indicator
import java.text.SimpleDateFormat
import javax.inject.Inject

class ActivityDetailActivity : BaseActivity(), ActivityDetailView {

    @Inject
    lateinit var activityDetailPresenter: ActivityDetailPresenter

    companion object {
        private const val INTENT_EXTRA_PARAM_ACTIVITY_ID = "INTENT_EXTRA_PARAM_ACTIVITY_ID"

        fun getCallingIntent(context: Context, activityId: String): Intent {
            val callingIntent = Intent(context, ActivityDetailActivity::class.java)
            callingIntent.putExtra(INTENT_EXTRA_PARAM_ACTIVITY_ID, activityId)
            return callingIntent
        }
    }

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_activity)

        AndroidInjection.inject(this)
    }

    override fun onStart() {
        super.onStart()

        activityDetailPresenter.attachView(this)

        activityDetailPresenter.start(currentActivityId())
    }
    //endregion

    private fun currentActivityId(): String? {
        return intent.extras.getString(INTENT_EXTRA_PARAM_ACTIVITY_ID)
    }

    //region ActivityDetailView implementation
    override fun showActivity(activityView: ActivityView) {
        Picasso.get().load(activityView.imageUrl).into(imageview_detail_thumbnail)

        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val date = format.parse(activityView.time)

        textview_detail_category.text = activityView.category
        textview_detail_name.text = activityView.name
        textview_detail_date.text = SimpleDateFormat("HH:mm dd/MM").format(date)
        textview_detail_age.text = activityView.ages
        textview_detail_description.text = activityView.description
        textview_detail_place.text = activityView.place
        textview_detail_address.text = getString(R.string.detail_address, activityView.streetName,
                activityView.postcode, activityView.town, activityView.country)
        Picasso.get().load(
                getString(R.string.static_map_url, activityView.latitude, activityView.longitude)
        ).into(imageview_detail_map)
    }

    override fun showLoading() {
        loading_indicator.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading_indicator.visibility = View.GONE
    }

    override fun displayError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
    //endregion
}