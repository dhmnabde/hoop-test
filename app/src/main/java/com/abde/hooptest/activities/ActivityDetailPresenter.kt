package com.abde.hooptest.activities

import com.abde.hooptest.common.BasePresenter
import com.abde.hooptest.domain.interactor.SingleUseCase
import com.abde.hooptest.domain.model.Activity
import com.abde.hooptest.mapper.ActivityMapper
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ActivityDetailPresenter @Inject constructor(
        private val getActivityUseCase: SingleUseCase<Activity, String?>,
        private val mapper: ActivityMapper) : BasePresenter<ActivityDetailView>() {

    fun start(currentActivityId: String?) {
        this.retrieveActivity(currentActivityId)
    }

    private fun retrieveActivity(currentActivityId: String?) {
        showViewLoading()
        getActivityUseCase.execute(GetActivityObserver(), currentActivityId)
    }

    private fun showViewLoading() {
        getMvpView()?.showLoading()
    }

    private fun hideViewLoading() {
        getMvpView()?.hideLoading()
    }

    private fun displayViewError(message: String) {
        getMvpView()?.displayError(message)
    }

    private fun showViewActivity(activity: Activity) {
        getMvpView()?.showActivity(mapper.mapToView(activity))
        hideViewLoading()
    }

    private inner class GetActivityObserver : DisposableSingleObserver<Activity>() {
        override fun onSuccess(activity: Activity) {
            showViewActivity(activity)
        }

        override fun onError(e: Throwable) {
            hideViewLoading()
            displayViewError(e.localizedMessage)
        }
    }
}