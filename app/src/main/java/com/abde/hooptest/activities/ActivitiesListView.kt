package com.abde.hooptest.activities

import com.abde.hooptest.model.ActivityView
import com.progtest.speedometer.common.MvpView

interface ActivitiesListView : MvpView {
    fun showActivities(activities: List<ActivityView>)
    fun showLoading()
    fun hideLoading()
    fun displayError(message: String)
}