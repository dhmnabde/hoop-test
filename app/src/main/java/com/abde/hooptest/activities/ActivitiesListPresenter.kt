package com.abde.hooptest.activities

import com.abde.hooptest.common.BasePresenter
import com.abde.hooptest.domain.interactor.SingleUseCase
import com.abde.hooptest.domain.model.Activity
import com.abde.hooptest.mapper.ActivityMapper
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ActivitiesListPresenter @Inject constructor(
        private val getActivitiesUseCase: SingleUseCase<List<Activity>, Unit?>,
        private val mapper: ActivityMapper) :
        BasePresenter<ActivitiesListView>() {

    fun start() {
        this.retrieveActivities()
    }

    private fun retrieveActivities() {
        showViewLoading()
        getActivitiesUseCase.execute(GetActivitiesObserver())
    }

    private fun showViewLoading() {
        getMvpView()?.showLoading()
    }

    private fun hideViewLoading() {
        getMvpView()?.hideLoading()
    }

    private fun displayViewError(message: String) {
        getMvpView()?.displayError(message)
    }

    private fun showViewActivities(activities: List<Activity>) {
        getMvpView()?.showActivities(activities.map { mapper.mapToView(it) })
        hideViewLoading()
    }

    private inner class GetActivitiesObserver : DisposableSingleObserver<List<Activity>>() {
        override fun onSuccess(activities: List<Activity>) {
            showViewActivities(activities)
        }

        override fun onError(e: Throwable) {
            hideViewLoading()
            displayViewError(e.localizedMessage)
        }
    }
}