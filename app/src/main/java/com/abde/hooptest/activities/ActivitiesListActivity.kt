package com.abde.hooptest.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.abde.hooptest.R
import com.abde.hooptest.activities.ActivityAdapter.OnItemClickListener
import com.abde.hooptest.common.BaseActivity
import com.abde.hooptest.model.ActivityView
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_list_activities.loading_indicator
import kotlinx.android.synthetic.main.activity_list_activities.recyclerView_activitieslist
import javax.inject.Inject

class ActivitiesListActivity : BaseActivity(), ActivitiesListView, OnItemClickListener {

    private lateinit var activityAdapter: ActivityAdapter

    @Inject
    lateinit var activitiesListPresenter: ActivitiesListPresenter

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_activities)

        AndroidInjection.inject(this)

        setupRecycler()
    }

    override fun onStart() {
        super.onStart()

        activitiesListPresenter.attachView(this)

        activitiesListPresenter.start()
    }
    //endregion

    private fun setupRecycler() {
        activityAdapter = ActivityAdapter()
        activityAdapter.listener = this

        recyclerView_activitieslist.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ActivitiesListActivity)
            adapter = activityAdapter

        }
    }

    //region ActivitiesListView implementation
    override fun showActivities(activities: List<ActivityView>) {
        activityAdapter.activities = activities
    }

    override fun showLoading() {
        loading_indicator.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading_indicator.visibility = View.GONE
    }

    override fun displayError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
    //endregion

    //region OnItemClickListener implementation
    override fun onItemClick(activityView: ActivityView) {
        navigator.navigateToDetail(this, activityView.id)
    }
    //endregion

}