package com.abde.hooptest.activities

import com.abde.hooptest.model.ActivityView
import com.progtest.speedometer.common.MvpView

interface ActivityDetailView : MvpView {
    fun showLoading()
    fun hideLoading()
    fun displayError(message: String)
    fun showActivity(activityView: ActivityView)

}