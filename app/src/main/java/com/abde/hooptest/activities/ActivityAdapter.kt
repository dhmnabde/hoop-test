package com.abde.hooptest.activities

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.abde.hooptest.R
import com.abde.hooptest.model.ActivityView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_activity.view.imageview_card_thumbnail
import kotlinx.android.synthetic.main.card_activity.view.textview_card_age
import kotlinx.android.synthetic.main.card_activity.view.textview_card_name
import kotlinx.android.synthetic.main.card_activity.view.textview_card_place
import kotlinx.android.synthetic.main.card_activity.view.textview_card_time
import java.text.SimpleDateFormat

class ActivityAdapter : RecyclerView.Adapter<ActivityAdapter.ViewHolder>() {

    var activities: List<ActivityView>? = null
        set(value) {
            field = value
            this.notifyDataSetChanged()
        }

    var listener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(activityView: ActivityView)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(activityView: ActivityView?,
                listener: OnItemClickListener?) = with(itemView) {
            if (activityView != null) {
                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = format.parse(activityView.time)

                textview_card_time.text = SimpleDateFormat("HH:mm dd/MM").format(date)
                textview_card_name.text = activityView.name
                textview_card_age.text = activityView.ages
                textview_card_place.text = activityView.place
                Picasso.get().load(activityView.imageUrl).fit().centerCrop().into(
                        imageview_card_thumbnail)
                itemView.setOnClickListener {
                    listener?.onItemClick(activityView)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_activity, parent,
                false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(activities?.get(position), listener)
    }

    override fun getItemCount(): Int {
        return activities?.count() ?: 0
    }
}