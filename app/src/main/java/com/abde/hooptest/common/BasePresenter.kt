package com.abde.hooptest.common

import com.progtest.speedometer.common.MvpView
import com.progtest.speedometer.common.Presenter

/**
 * Abstract presenter that every other Presenter in this application must implement.
 */
open class BasePresenter<T : MvpView> : Presenter<T> {
    private var mvpView: T? = null

    override fun attachView(mvpView: T) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        this.mvpView = null
    }

    fun isViewAttached(): Boolean {
        return this.mvpView != null
    }

    fun getMvpView(): T? {
        return this.mvpView
    }
}