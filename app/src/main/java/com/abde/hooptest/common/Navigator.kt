package com.abde.hooptest.common

import android.content.Context
import com.abde.hooptest.activities.ActivityDetailActivity
import com.abde.hooptest.di.scopes.PerApplication
import javax.inject.Inject

@PerApplication
class Navigator @Inject constructor() {

    fun navigateToDetail(context: Context?, activityId: String) {
        if (context != null) {
            val intentToLaunch = ActivityDetailActivity.getCallingIntent(context, activityId)
            context.startActivity(intentToLaunch)
        }
    }
}