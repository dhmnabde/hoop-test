package com.abde.hooptest.common

import android.support.v7.app.AppCompatActivity
import com.abde.hooptest.common.Navigator
import javax.inject.Inject

/**
 * Abstract activity that every other Activity in this application must implement.
 */
abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var navigator: Navigator
}