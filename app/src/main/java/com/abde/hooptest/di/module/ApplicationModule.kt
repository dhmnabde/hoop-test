package com.abde.hooptest.di.module

import android.app.Application
import android.content.Context
import com.abde.hooptest.data.ActivityDataStoreFactory
import com.abde.hooptest.data.mapper.ActivityMapper
import com.abde.hooptest.data.repository.ActivityDataRepository
import com.abde.hooptest.data.source.ServiceGenerator
import com.abde.hooptest.di.scopes.PerApplication
import com.abde.hooptest.domain.executor.PostExecutionThread
import com.abde.hooptest.domain.repository.ActivityRepository
import com.abde.hooptest.common.UiThread
import dagger.Module
import dagger.Provides

@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @PerApplication
    internal fun provideSetRepository(factory: ActivityDataStoreFactory,
            mapper: ActivityMapper): ActivityRepository {
        return ActivityDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideServiceGenerator(): ServiceGenerator {
        return ServiceGenerator()
    }
}