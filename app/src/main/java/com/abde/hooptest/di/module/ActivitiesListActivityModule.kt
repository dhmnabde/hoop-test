package com.abde.hooptest.di.module

import com.abde.hooptest.activities.ActivitiesListPresenter
import com.abde.hooptest.di.scopes.PerActivity
import com.abde.hooptest.domain.interactor.activities.GetActivities
import com.abde.hooptest.mapper.ActivityMapper
import dagger.Module
import dagger.Provides

@Module
open class ActivitiesListActivityModule {
    @PerActivity
    @Provides
    internal fun provideActivitiesListPresenter(
            getActivities: GetActivities, mapper: ActivityMapper): ActivitiesListPresenter {
        return ActivitiesListPresenter(getActivities, mapper)
    }
}