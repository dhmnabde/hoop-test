package com.abde.hooptest.di.module

import com.abde.hooptest.activities.ActivitiesListActivity
import com.abde.hooptest.activities.ActivityDetailActivity
import com.abde.hooptest.di.scopes.PerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [(ActivitiesListActivityModule::class)])
    abstract fun bindActivitiesListActivity(): ActivitiesListActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [(ActivityDetailActivityModule::class)])
    abstract fun bindActivityDetailActivity(): ActivityDetailActivity
}