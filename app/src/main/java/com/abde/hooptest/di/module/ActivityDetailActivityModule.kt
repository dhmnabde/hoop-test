package com.abde.hooptest.di.module

import com.abde.hooptest.activities.ActivityDetailPresenter
import com.abde.hooptest.di.scopes.PerActivity
import com.abde.hooptest.domain.interactor.activities.GetActivity
import com.abde.hooptest.mapper.ActivityMapper
import dagger.Module
import dagger.Provides

@Module
open class ActivityDetailActivityModule {
    @PerActivity
    @Provides
    internal fun provideActivityDetailPresenter(
            getActivity: GetActivity, mapper: ActivityMapper): ActivityDetailPresenter {
        return ActivityDetailPresenter(getActivity, mapper)
    }
}