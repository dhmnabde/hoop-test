package com.abde.hooptest.di

import android.app.Application
import com.abde.hooptest.common.AppApplication
import com.abde.hooptest.di.module.ActivityBindingModule
import com.abde.hooptest.di.module.ApplicationModule
import com.abde.hooptest.di.scopes.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
        modules = [(ActivityBindingModule::class), (ApplicationModule::class), (AndroidSupportInjectionModule::class)])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: AppApplication)
}