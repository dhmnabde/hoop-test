package com.abde.hooptest.mapper

import com.abde.hooptest.domain.model.Activity
import com.abde.hooptest.model.ActivityView
import javax.inject.Inject

class ActivityMapper @Inject constructor() : Mapper<ActivityView, Activity> {
    override fun mapToView(type: Activity): ActivityView {
        return ActivityView(type.id,
                type.time,
                type.name,
                type.imageUrl,
                type.ages,
                type.place,
                type.category,
                type.description,
                type.latitude,
                type.longitude,
                type.country,
                type.postcode,
                type.streetName,
                type.town)
    }
}