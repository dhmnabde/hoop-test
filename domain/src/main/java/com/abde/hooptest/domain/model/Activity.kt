package com.abde.hooptest.domain.model

class Activity(val id: String,
        val time: String,
        val name: String,
        val imageUrl: String,
        val ages: String,
        val place: String,
        val category: String,
        val description: String,
        val latitude: String,
        val longitude: String,
        val country: String,
        val postcode: String,
        val streetName: String,
        val town: String)