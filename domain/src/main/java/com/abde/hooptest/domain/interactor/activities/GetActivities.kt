package com.abde.hooptest.domain.interactor.activities

import com.abde.hooptest.domain.executor.PostExecutionThread
import com.abde.hooptest.domain.interactor.SingleUseCase
import com.abde.hooptest.domain.model.Activity
import com.abde.hooptest.domain.repository.ActivityRepository
import io.reactivex.Single
import javax.inject.Inject

class GetActivities @Inject constructor(private val activityRepository: ActivityRepository,
        postExecutionThread: PostExecutionThread) :
        SingleUseCase<List<Activity>, Unit?>(postExecutionThread) {
    override fun buildUseCaseObservable(params: Unit?): Single<List<Activity>> {
        return activityRepository.getActivities()
    }
}