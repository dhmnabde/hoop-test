package com.abde.hooptest.domain.repository

import com.abde.hooptest.domain.model.Activity
import io.reactivex.Single

interface ActivityRepository {
    fun getActivities(): Single<List<Activity>>
    fun getActivity(activityId: String): Single<Activity>
}