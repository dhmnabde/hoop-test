package com.abde.hooptest.domain.executor

import io.reactivex.Scheduler

/**
 * Thread abstraction created to change the execution context from any thread to any other thread.
 * Use to encapsulate a UI Thread.
 */
interface PostExecutionThread {
    val scheduler: Scheduler
}