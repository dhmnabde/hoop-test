package com.abde.hooptest.domain.interactor.activities

import com.abde.hooptest.domain.executor.PostExecutionThread
import com.abde.hooptest.domain.interactor.SingleUseCase
import com.abde.hooptest.domain.model.Activity
import com.abde.hooptest.domain.repository.ActivityRepository
import io.reactivex.Single
import java.security.InvalidParameterException
import javax.inject.Inject

class GetActivity @Inject constructor(private val activityRepository: ActivityRepository,
        postExecutionThread: PostExecutionThread) :
        SingleUseCase<Activity, String?>(postExecutionThread) {
    override fun buildUseCaseObservable(params: String?): Single<Activity> {
        if (params == null) {
            return Single.error(InvalidParameterException("Invalid Activity Id"))
        }
        return activityRepository.getActivity(params)
    }
}