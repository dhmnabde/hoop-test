# Hoop test
Application using an API from Hoop to display activities

Architecture used: Clean architecture

Go to [dependencies.gradle](/config/buildsystem/dependencies.gradle) to see which libraries are used.

to launch the application, pull the repository, then launch the project with android studio (version used: 3.1.2)